/**
 * Created by ABC on 2/27/2015.
 */
var nodemailer = require('nodemailer');
var sendResponse=require('./sendResponse');
var autht=config.get('emailSettings');

exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);
    console.log(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == ''||arr[i] == undefined||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}
exports.sendMail=function(emailTo,subName,message,res) {


   /* var smtpTransport = nodemailer.createTransport("Direct", {debug: true}

    );*/
    var smtpTransport=nodemailer.createTransport("SMTP",{
        service:"Gmail",
        auth:{
            user:autht.email,
            pass:autht.password
        }
    });
    var mailOptions = {
        //from: autht.email,
        to: emailTo,
        subject: subName,
        text: message
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            sendResponse.somethingWentWrongError(res);
        } else {
            sendResponse.sendSuccessFullyMail(res);
        }
    });

}