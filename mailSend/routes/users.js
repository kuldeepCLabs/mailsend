var express = require('express');
var router = express.Router();
var sendResponse=require('./sendResponse');
var func=require('./commonfunction');
var async=require('async');
//var validator=require('email-validator');
var request=require('request');
/* GET users listing. */
router.post('/send_mail', function(req, res, next) {

  var emailTo=req.body.email_to;
  var subName=req.body.sub_name;
  var message=req.body.message;

  var manValues=[emailTo,subName,message];
    async.waterfall([
        function(callback){
           func.checkBlank(res,manValues,callback);
        },
        function(callback){
          checkEmailValidation(res,emailTo,callback);
        }
    ],function(err,result){
            if(err){
                sendResponse.somethingWentWrongError(res);
            }
            else{
                func.sendMail(emailTo,subName,message,res);
            }
        }
    );
});
function checkEmailValidation(ress,email,callback){

      request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: email},
        json: true
    }, function (err,res,body) {
          console.log(body.valid);
          if (body.valid) {
              callback(null);
          }
          else {
              sendResponse.invalidEmail(ress);
          }

      });
}

module.exports = router;
